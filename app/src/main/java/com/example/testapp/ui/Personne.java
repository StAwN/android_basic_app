package com.example.testapp.ui;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Personne implements Parcelable {
    private String nom;
    private String prenom;
    private int age;
    public static ArrayList<Personne> persons;

    public Personne(String nom, String prenom, int age) {
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
    }

    protected Personne(Parcel in) {
        this.nom = in.readString();
        this.prenom = in.readString();
        this.age = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nom);
        dest.writeString(prenom);
        dest.writeInt(age);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Personne> CREATOR = new Creator<Personne>() {
        @Override
        public Personne createFromParcel(Parcel in) {
            return new Personne(in);
        }

        @Override
        public Personne[] newArray(int size) {
            return new Personne[size];
        }
    };

    //getters
    public String getNom() {
        return nom; // this. inutile // getId: nom variable mais getMaj par convention
    }
    public String getPrenom() {
        return prenom;
    }
    public int getAge() {
        return age;
    }

    //setters
    public void setNom(String nom) {
        this.nom = nom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }
    public void setAge(int age) {
        this.age = age;
    }

    //Autre méthodes
    public static ArrayList<Personne> getPersonList() {
        ArrayList<Personne> results = new ArrayList<Personne>();
        results.add(new Personne("Versaires", "Annie",42));
        results.add(new Personne("Hatant", "Charles",25));
        results.add(new Personne("Bonnot", "Jean",54));
        return results;
    }
}
