package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Parcel;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.testapp.ui.Personne;

import java.util.ArrayList;

public class Main3Activity extends AppCompatActivity {

    ListView lvListe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        String receivedMessage;
        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            receivedMessage = extras.getString("MESSAGE");
            TextView t = (TextView) findViewById(R.id.myText);
            t.setText(receivedMessage);

            Personne qqn = extras.getParcelable("PERSONNE");
            if (qqn != null) {
                TextView displayPerson = (TextView) findViewById(R.id.permsg);
                displayPerson.setText("Person : " + "\n" + " FirstName : " +
                        qqn.getPrenom() + "\n" + " LastName : " + qqn.getNom() +
                        "\n" + " Age : " + qqn.getAge());
            }
            ArrayList<Personne> persons =extras.getParcelableArrayList("LISTE");
            if (persons != null){
                int i = 0;
                lvListe = (ListView)findViewById(R.id.lvListe);
                String[] listeStrings = new String[3];
                //Boucle
                for(Personne d: persons) {
                    System.out.println(d.getNom());
                    Log.v("PERSONNE", d.getNom()); // 2e affichage
                    listeStrings[i] = d.getNom() + ", " + d.getPrenom() + ", " + d.getAge();
                    i++;
                }
                lvListe.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,listeStrings));
            }
        }
    }
}