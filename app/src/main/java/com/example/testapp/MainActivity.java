package com.example.testapp;

import static com.example.testapp.R.layout.activity_main;
import static com.example.testapp.R.layout.activity_main2;
import static com.example.testapp.ui.Personne.persons;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.testapp.ui.Livre;
import com.example.testapp.ui.LivreAdapter;
import com.example.testapp.ui.Personne;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static ArrayList<Personne> getPersonList() {
        ArrayList<Personne> results = new ArrayList<Personne>();
        results.add(new Personne("Versaires", "Annie",42));
        results.add(new Personne("Hatant", "Charles",25));
        results.add(new Personne("Bonnot", "Jean",54));
        return results;
    }

    public static ArrayList<Personne> persons;

    ListView lvListe;
    List<Livre> maBibliotheque = new ArrayList<Livre>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(activity_main);

        Button b1 = ((Button) findViewById(R.id.btn1));// utiliser findViewById()
        b1.setOnClickListener(this);// appliquer à b1 un listener de clicks

        Button b2 = ((Button) findViewById(R.id.btn2));// utiliser findViewById()
        b2.setOnClickListener(this);// appliquer à b2 un listener de clicks

        Button b3 = ((Button) findViewById(R.id.btn3));// utiliser findViewById()
        b3.setOnClickListener(this);// appliquer à b3 un listener de clicks

        Button b4 = ((Button) findViewById(R.id.btn4));// utiliser findViewById()
        b4.setOnClickListener(this);// appliquer à b4 un listener de clicks

        lvListe = (ListView)findViewById(R.id.lvListe);
        RemplirLaBibliotheque();
        LivreAdapter adapter = new LivreAdapter(this, maBibliotheque);
        lvListe.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private void RemplirLaBibliotheque() {
        maBibliotheque.clear();
        maBibliotheque.add(new Livre("La vraie histoire d’Amélie Poulain", "Jean Cheval"));
        maBibliotheque.add(new Livre("L’assomoir", "Emile Zola"));
        maBibliotheque.add(new Livre("L’immoraliste", "André Gide"));
    }

    @Override
    public void onClick(View view) {
        System.out.println("click done");
        Intent intent;
        Intent intent3;
        Intent intent4;
        Intent intent5;

        switch(view.getId()){
            case R.id.btn1:
                //on précise ici : le premier argument pour l’objet Intentest l'activité de départ,
                //et le second argument est la classe reliée à l'activité ciblée
                intent = new Intent(this, Main3Activity.class);
                startActivity(intent);
                break;

            case R.id.btn2:
                EditText test = ((EditText) findViewById(R.id.edit));
                String message = test.getText().toString();
                //on précise ici : le premier argument pour l’objet Intentest l'activité de départ,
                //et le second argument est la classe reliée à l'activité ciblée
                intent3 = new Intent(this, Main3Activity.class);
                intent3.putExtra("MESSAGE", message);//message est la texte de l’EditText
                startActivity(intent3);
                break;

            case R.id.btn3:
                Personne qqn = new Personne("Versaire", "Annie", 42);
                intent4 = new Intent(this, Main3Activity.class);
                intent4.putExtra("PERSONNE", qqn);
                startActivity(intent4);
                break;

            case R.id.btn4:
                Bundle bundle = new Bundle();
                //Personne [] tab;
                //Personne j1 = new Personne("Versaires", "Annie", 42);
                //Personne j2 = new Personne("Ver", "Anne", 43);
                //tab[0] = j1;
                //tab[1] = j2;
                //bundle.putParcelableArrayList("LISTE", tab);
                bundle.putParcelableArrayList("LISTE", persons = getPersonList());
                intent5 = new Intent(this, Main3Activity.class);
                intent5.putExtras(bundle);
                startActivity(intent5);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + view.getId());
        }
    }
}
